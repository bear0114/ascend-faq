
#### 3.[ERROR] TBE(23053,atc.bin):[SecondStageFeatureExtractor/InceptionV2/Mixed_5a/Branch_0/Conv2d_0a_1x1/Conv2D] "in_channels(>0) should be divisible by kernel_channels when groups = 1."
     **问题定位** 因为FasterRcnn_resnet.pb模型中存在merge算子不支持动态shape，所以尝试V1转V2函数类算子。
     **解决办法** V1转V2函数类算子，再模型转换，
参考链接：https://gitee.com/ascend/tensorflow/issues/I3EEVT#note_4735950
参考链接：https://support.huaweicloud.com/atctool-cann330alpha2infer/atlasatc_16_0110.html