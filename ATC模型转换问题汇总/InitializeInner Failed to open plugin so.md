# Ascend-FAQ
 **onnx算子功能及各参数含义等基础知识请自查onnx算子库**  https://github.com/onnx/onnx/blob/master/docs/Operators.md

#### 介绍
Ascend 预处理、模型转换、后处理等相关问题汇总  **推理部署wiki** : https://gitee.com/ascend/samples/wikis  **模型训练wiki** :https://gitee.com/ascend/modelzoo/wikis

#### 软件架构
CANN 3.0


#### ATC模型转换

#### 1.【ERROR】FE:[tbe_op_store_adapter.cc:858]43824 InitializeInner:"Failed to open plugin so."  
 **详细描述** ：CANN-toolkit安装完成后，环境变量配置完成，模型转换报错；
![输入图片说明](https://images.gitee.com/uploads/images/2021/0412/201754_da3ff2ec_8049142.png "屏幕截图.png")

 **定位关键** ：python使用的anaconda的环境变量问题
![输入图片说明](https://images.gitee.com/uploads/images/2021/0412/202252_7ead5b36_8049142.png "屏幕截图.png")

 **解决办法** ：修改python环境变量解决
![输入图片说明](https://images.gitee.com/uploads/images/2021/0412/202150_8b932498_8049142.png "屏幕截图.png")

