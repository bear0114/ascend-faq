IBMC严重告警：
1. Restart the server and open BIOS Device Manager in UEFI mode. Enter the Driver Health screen, and select Repair the whole platform.
2. Contact Huawei technical support.
3. Replace the RAID controller card.
重启服务器，进入“Device Manager”界面，检查界面是否显示“The platform is healthy”。
#### 解决办法：
参考链接 https://support.huawei.com/enterprise/zh/doc/EDOC1100115323/216a6766  
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/175415_6284cea5_8049142.png "屏幕截图.png")  
重启过程中，当出现如图所示界面时，按“F11”  
选择“Device Manager”并按“Enter”  
选择“Avago MegaRAID <SAS3508> Configuration Utility”，按“Enter”->“main menu”->“configuration management”   
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/175450_dfe0ab9e_8049142.png "屏幕截图.png")    
创建RAID 0  
在下方的硬盘列表中选择要加入的成员盘并按“Enter”    
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/175842_51d2941a_8049142.png "屏幕截图.png")   
退出>选择“Virtual Drive Management”并按“Enter”。  
显示当前存在的RAID信息。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/1108/175919_1fae97cd_8049142.png "屏幕截图.png")  
RAID组成功，下一步就是很简单的搭建操作系统环境了。