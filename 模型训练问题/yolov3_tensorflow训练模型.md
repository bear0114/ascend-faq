
modelzoo链接：
#### 问题描述：1.安装os依赖报错  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0531/193603_2dde2e54_8049142.png "屏幕截图.png")  
#### 解决办法：将镜像源地址https改成http  

#### 问题描述：2.启动脚本docker_start.sh 注释掉不用的dvices报错   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0531/193757_be13e0e1_8049142.png "屏幕截图.png")   
#### 解决办法：如果注释就会报这个错，需要把docker_start.sh文件中多余的device删掉    

#### 问题描述：3.  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0531/193957_ab4619ba_8049142.png "屏幕截图.png")   
#### 解决办法：Numpy版本太高导致的，降低numpy版本至1.18.0以下就可以    


#### 问题描述：4.   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0531/194058_0bebafee_8049142.png "屏幕截图.png")  
