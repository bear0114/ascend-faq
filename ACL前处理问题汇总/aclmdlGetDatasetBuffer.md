##### 2.aclmdlGetDatasetBuffer

 **详细描述：** 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/105238_63ab1296_8049142.png "aclmdlgetdatasetbuffer.png")  

 **定位关键：** 
从log打印中可以看到是aclmdlGetDataseBuffer函数报错，index值为2，可以知道说明是获取的是第2个aclDataBuffer输入有问题，可以从应用开发指导文件中查到  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/105639_4b924acb_8049142.png "屏幕截图.png")

 **解决办法：** 
因为Fasterncc-dynamic网络是双输入
第1个dynamic输入该接口如图：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/105742_e3545406_8049142.png "屏幕截图.png")

参考YOLOV3刚开始双输入第2个输入接口和第1个选的一样的，导致错误,后来换了单输入接口后不报错了
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/105835_1002e438_8049142.png "屏幕截图.png")